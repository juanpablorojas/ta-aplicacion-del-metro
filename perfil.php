<? session_start() ?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Perfil</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="js/vendor/jquery-1.11.3.min.js"></script>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/registro.css">
    </head>
    <body>
        <div class="container">
            <h1>Cambiar Información del Perfil</h1>
            <br><br>
            <form action="scripts php/actualizarPerfil.php" method="post" id="frm">
                <div class="form-group">
                    <h3><label>Nombre actual: 
                        <?php echo $_SESSION['username'] ?>
                    </label></h3>
                    <br>
                    <input name="nombre" type="text" class="form-control campo" placeholder="Cambiar nombre">
                </div>
                <div class="form-group">
                    <h3><label for="pwd">Ingrese nueva contraseña:</label></h3>
                    <br>
                    <input name="pass" type="password" class="form-control campo" placeholder="Ingrese Contraseña">
                </div>
                <div class="form-group">
                    <h3><label for="pwd">Confirme la nueva contraseña:</label></h3>
                    <br>
                    <input name="pass2" type="password" class="form-control campo" placeholder="Confirme Contraseña">
                </div>
                <br>
                <div class="form-group">
                    <h3 style="font-size:30px"><label for="pwd">Cambiar tipo de cívica:</label></h3>
                    <br>
                    <select name="tipo" class="opciones form-control campo" value='6'>
                      <option value="1">Frecuente</option>
                      <option value="2">Adulto Mayor</option>
                      <option value="3">Estudiantil Municipios</option>
                      <option value="4">PMR</option>
                      <option value="5">Al Portador</option>
                      <option value="6" selected>------</option>
                    </select>
                </div>
                <br>
                <input class="opciones btn btn-success" type="submit" value="Realizar Cambios"></input>
            </form>
        </div>
    </body>
</html>