<? session_start() ?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<script src="js/vendor/jquery-1.11.3.min.js"></script>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/beneficios.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/publicidad.css">
	<script type="text/javascript" src="js/animaciones.js"></script>
	<script type="text/javascript" src="js/publicidad.js"></script>
	<script src="js/vendor/TweenMax.min.js"></script>
</head>
<body>
	<!--Barra de Inicio de sesión o Perfil-->
	<div id="contenidosUser">
		<span class="log" style="margin-right:10%" id="saludoLogin" onclick="window.location='scripts php/redir.php'">
			<?php
				if($_SESSION['username']): echo "¡Hola ".$_SESSION['username'].'!';
				else: echo "Iniciar Sesión";
				endif;
			?>
		</span>
		<span class="log" style="margin-left:10%" id="cerrarRegistro" onclick="window.location='scripts php/redirSesion.php'">
			<?php
				if($_SESSION['username']): echo "Cerrar Sesión";
				else: echo "Registrarse";
				endif;
			?>
		</span>
	</div>
	<div id="contenedor-beneficios">
		<header class="row">
			<div id="marca1" class="marcaAnimada col-sm-4"></div>			
			<div id="titulo" class="col-sm-4"><p><span id="titulo-1">Metro de Medellin</span></br><span id="titulo-2">Calidad de vida</span></p></div>
			<div id="marca2" class="marcaAnimada col-sm-4"></div>
		</header>
		<div class="division row">
			<br>
			<div id="icono1" class="iconos col-sm-4"><b><p class="texto">Investigamos e innovamos en compañía de universidades y proveedores locales (I+D+i)</p></b></div>
			<div id="icono2" class="iconos col-sm-4"><b><p class="texto">El METRO va más allá de ser un medio de transporte; es una forma de vida y un espacio para el encuentro (RSE)</p></b></div>
			<div id="icono3" class="iconos col-sm-4"><b><p class="texto">Somos un Sistema socialmente incluyente y ambientalmente sostenible (Movilidad Sostenible)</p></b></div>
		</div>
		<nav>
			<a href="index.php">Inicio</a>
			<a href="tarifas_y_horarios.php">Tarifas y horarios</a>
			<a href="monitoreo.php">Monitoreo</a>
			<a href="beneficios.php">Beneficios</a>
		</nav>
		<section>
			<div id="contenedorBeneficios">				
				<div class="advertisement" id="left-pane">
		            <p class="message" id="message1">Todos debemos respetar el sistema de señalizacion para demostrar una buena educacion y asi llegar a ser un mejor pais</p>
		            <p class="message" id="message2">Sigamos fortaleciendo nuestra cultura metro para tener una mejor cidudad y seguir mejorando para el futuro de nuestro pais</p>
		            <p class="message" id="message3">Si cumples con la estrategia de señalizacion, podras ahorrar dinero en tus viajes</p>
		            <p class="message" id="message4">Si sigues con la estrategia de la senalizacion de los vagones, ya no tendras que preocuparte mas por como salir del vagon</p>
		        </div>
				<div id="tabla1">
					<h1 class="tituloContenido">Beneficios</h1>
					<!--La siguiente tabla se muestra si el usuario no está loggeado-->
					<?php if(!$_SESSION['username']): ?>
					<table class="tablaBeneficios">
						<tr><th class="encabezado1">Perfil</th><th class="encabezado1 tarifa">Descuento por usar el servicio</th></tr>
						<tr><td class="">Frecuente</td><td class="tarifa">10% en la tarifa</td></tr>
						<tr><td class="">Adulto Mayor</td><td class="tarifa">15% en la tarifa</td></tr>
						<tr><td class="">Estudiantil Municipios</td><td class="tarifa">35% en la tarifa</td></tr>
						<tr><td class="">PMR</td><td class="tarifa">25% en la tarifa</td></tr>
						<tr><td class="">Al Portador</td><td class="tarifa">5% en la tarifa</td></tr>
						<tr><th colspan="2" id="encabezado2">Condiciones de Aplicación</th></tr>
						<tr><th colspan="2">Los descuentos se dan por el uso de la aplicación interactiva.</th></tr>
						<tr><th colspan="2">Si usted desea acceder a un descuento, debe indicar qué estación va a visitar en un horario despejado (Pestaña de "Monitoreo")</th></tr>
						<!-- <tr><th colspan="2">Cuando la aplicación detecte que usted ha cumplido con esta condición, tendrá unas opciones que usted podrá escoger en su cuenta, en esta misma sección, y su saldo a favor se verá reflejado</th></tr>	 -->
					</table>
					<?php else: ?>
					<!--La siguiente tabla se muestra si el usuario está loggeado-->
					<table class="table table-hover">
					    <thead style="text-align:center">
					      <tr>
					        <th>Campo</th>
					        <th>Valor</th>
					      </tr>
					    </thead>
					    <tbody>
					      <tr>
					        <td>Usuario</td>
					        <td class="dato">
					        	<?php echo $_SESSION['username'] ?>
					        </td>
					      </tr>
					      <tr>
					        <td>Tipo de Cívica</td>
					        <td class="dato">
					        	<?php  
					        		switch ($_SESSION['usercivica']){
					        			case 1:
					        				echo 'Frecuente';
					        				break;
				        				case 2:
				        					echo 'Adulto Mayor';
				        					break;
			        					case 3:
			        						echo 'Estudiantil Municipios';
			        						break;
		        						case 4:
		        							echo 'PMR';
		        							break;
	        							case 5:
	        								echo 'Al Portador';
	        								break;

					        		}
					        	?>
					        </td>
					      </tr>
					      <tr>
					        <td>Saldo</td>
					        <td class="dato">
					        	<?php
						        	include("scripts php/conexion.php");
									$con = mysql_connect($host,$user,$pw);
									$state = mysql_select_db($db,$con);

									$query = mysql_query("SELECT saldo FROM usuarios 
										WHERE nombre='$_SESSION[username]' ", $con);

									$data = mysql_fetch_array($query);
									echo "$".$data['saldo'];
								?>
					        </td>
					      </tr>
					      <tr>
					        <td>Veces que ha bonificado</td>
					        <td class="dato">
					        	<?php
						        	include("scripts php/conexion.php");
									$con = mysql_connect($host,$user,$pw);
									$state = mysql_select_db($db,$con);

									$query = mysql_query("SELECT bonificaciones FROM usuarios 
										WHERE nombre='$_SESSION[username]' ", $con);

									$data = mysql_fetch_array($query);
									echo $data['bonificaciones'];
								?>
					        </td>
					      </tr>
					      <tr>
					        <td>Hora de la última bonificación</td>
					        <td class="dato">
					        	<?php
									echo $_SESSION['horabonificacion'].':'.$_SESSION['minbonificacion'];
								?>
					        </td>
					      </tr>
					    </tbody>
					</table>
					<?php endif; ?>
				</div>
				<div class="advertisement" id="right-pane">
		            <p class="message" id="message1">Sigamos fortaleciendo nuestra cultura metro para tener una mejor cidudad y seguir mejorando para el futuro de nuestro pais</p>
		            <p class="message" id="message2">Si sigues con la estrategia de la senalizacion de los vagones, ya no tendras que preocuparte mas por como salir del vagon</p>
		            <p class="message" id="message3">Todos debemos respetar el sistema de señalizacion para demostrar una buena educacion y asi llegar a ser un mejor pais</p>
		            <p class="message" id="message4">Si cumples con la estrategia de señalizacion, podras ahorrar dinero en tus viajes</p>
		        </div>
			</div>
			
		</section>
		<hr>
		<hr>
		<footer></footer>
	</div>
</body>
</html>