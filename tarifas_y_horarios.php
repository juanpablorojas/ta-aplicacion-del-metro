<? session_start() ?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<script src="js/vendor/jquery-1.11.3.min.js"></script>
	<link rel="stylesheet" href="css/tarifas.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="js/mover_tarifas.js"></script>
	<script type="text/javascript" src="js/animaciones.js"></script>
	<script src="js/vendor/TweenMax.min.js"></script>
</head>
<body>
	<!--Barra de Inicio de sesión o Perfil-->
	<div id="contenidosUser">
		<span class="log" style="margin-right:10%" id="saludoLogin" onclick="window.location='scripts php/redir.php'">
			<?php
				if($_SESSION['username']): echo "¡Hola ".$_SESSION['username'].'!';
				else: echo "Iniciar Sesión";
				endif;
			?>
		</span>
		<span class="log" style="margin-left:10%" id="cerrarRegistro" onclick="window.location='scripts php/redirSesion.php'">
			<?php
				if($_SESSION['username']): echo "Cerrar Sesión";
				else: echo "Registrarse";
				endif;
			?>
		</span>
	</div>
	<div id="contenedor-tarifas">
		<header class="row">
			<div id="marca1" class="marcaAnimada col-sm-4"></div>			
			<div id="titulo" class="col-sm-4"><p><span id="titulo-1">Metro de Medellin</span></br><span id="titulo-2">Calidad de vida</span></p></div>
			<div id="marca2" class="marcaAnimada col-sm-4"></div>
		</header>
		<div class="division row">
			<br>
			<div id="icono1" class="iconos col-sm-4"><b><p class="texto">Investigamos e innovamos en compañía de universidades y proveedores locales (I+D+i)</p></b></div>
			<div id="icono2" class="iconos col-sm-4"><b><p class="texto">El METRO va más allá de ser un medio de transporte; es una forma de vida y un espacio para el encuentro (RSE)</p></b></div>
			<div id="icono3" class="iconos col-sm-4"><b><p class="texto">Somos un Sistema socialmente incluyente y ambientalmente sostenible (Movilidad Sostenible)</p></b></div>				
		</div>	
		<nav>
			<a href="index.php">Inicio</a>
			<a href="tarifas_y_horarios.php">Tarifas y horarios</a>
			<a href="monitoreo.php">Monitoreo</a>
			<a href="beneficios.php">Beneficios</a>
		</nav>
		<section>
			<div id="contenedorTarifas">
				<h1 class="tituloContenido">Tarifas 2015</h1>
				<div id="tabla1">
					<table class="tablaTarifas" id="perfil1">
						<tr><th class="encabezado1 perfil">Perfil</th><th class="encabezado1 tarifa">Tarifa</th></tr>
						<tr><td class="perfil">Frecuente</td><td class="tarifa">$1.750</td></tr>
						<tr><td class="perfil">Adulto Mayor</td><td class="tarifa">$1.660</td></tr>
						<tr><td class="perfil">Estudiantil Municipios</td><td class="tarifa">$910</td></tr>
						<tr><td class="perfil">PMR</td><td class="tarifa">$1.340</td></tr>
						<tr><td class="perfil">Al Portador</td><td class="tarifa">$1.810</td></tr>
						<tr><th colspan="2" id="encabezado2">Modos de viaje</th></tr>
						<tr><th colspan="2"><img src="img/cable.jpg" class="iconos-tarifas" data-toggle="tooltip" title="Cable">+
							<img src="img/metro.jpg" alt="" class="iconos-tarifas" data-toggle="tooltip" title="Metro">+
							<img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metroplus.jpg" alt="" title="Metroplús" data-toggle="tooltip" class="iconos-tarifas"></th></tr>
						<tr><th colspan="2"><img src="img/cable.jpg" alt="" title="Cable" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metro.jpg" alt="" title="Metro" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metroplus.jpg" alt="" title="Metroplús" data-toggle="tooltip" class="iconos-tarifas"></th></tr>
						<tr><th colspan="2"><img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metroplus.jpg" alt="" title="Metroplús" data-toggle="tooltip" class="iconos-tarifas"></th></tr>
					</table>
				</div>	
				
				<div id="tabla2">
					<table class="tablaTarifas" id="perfil2">
						<tr><th class="encabezado1 perfil">Perfil</th><th class="encabezado1 tarifa">Tarifa</th></tr>
						<tr><td class="perfil">Frecuente</td><td class="tarifa">$2.150</td></tr>
						<tr><td class="perfil">Adulto Mayor</td><td class="tarifa">$2.040</td></tr>
						<tr><td class="perfil">Estudiantil Municipios</td><td class="tarifa">$1.120</td></tr>
						<tr><td class="perfil">PMR</td><td class="tarifa">$1.650</td></tr>
						<tr><td class="perfil">Al Portador</td><td class="tarifa">$2.210</td></tr>
						<tr><th colspan="2" id="encabezado3">Modos de viaje</th></tr>
						<tr><th colspan="2"><img src="img/cable.jpg" alt="" title="Cable" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metro.jpg" alt="" title="Metro" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas"></th></tr>
						<tr><th colspan="2"><img src="img/cable.jpg" alt="" title="Cable" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metro.jpg" alt="" title="Metro" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metroplus.jpg" alt="" title="Metroplús" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas"></th></tr>
						
					</table>
				</div>

				<div id="tabla3">
					<table class="tablaTarifas" id="perfil3">
						<tr><th class="encabezado1 perfil">Perfil</th><th class="encabezado1 tarifa">Tarifa</th></tr>
						<tr><td class="perfil">Frecuente</td><td class="tarifa">$2.600</td></tr>
						<tr><td class="perfil">Adulto Mayor</td><td class="tarifa">$2.460</td></tr>
						<tr><td class="perfil">Estudiantil Municipios</td><td class="tarifa">$1.360</td></tr>
						<tr><td class="perfil">PMR</td><td class="tarifa">$2.000</td></tr>
						<tr><td class="perfil">Al Portador</td><td class="tarifa">$2.660</td></tr>
						<tr><th colspan="2" id="encabezado4">Modos de viaje</th></tr>
						<tr><th colspan="2"><img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metroplus.jpg" alt="" title="Metroplús" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas"></th></tr>
					</table>
				</div>	

				<div id="tabla4">
					<table class="tablaTarifas" id="perfil4">
						<tr><th class="encabezado1 perfil">Perfil</th><th class="encabezado1 tarifa">Tarifa</th></tr>
						<tr><td class="perfil">Frecuente</td><td class="tarifa">$3.000</td></tr>
						<tr><td class="perfil">Adulto Mayor</td><td class="tarifa">$2.840</td></tr>
						<tr><td class="perfil">Estudiantil Municipios</td><td class="tarifa">$1.570</td></tr>
						<tr><td class="perfil">PMR</td><td class="tarifa">$2.310</td></tr>
						<tr><td class="perfil">Al Portador</td><td class="tarifa">$3.060</td></tr>
						<tr><th colspan="2" id="encabezado5">Modos de viaje</th></tr>
						<tr><th colspan="2"><img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metroplus.jpg" alt="" title="Metroplús" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/cable.jpg" alt="" title="Cable" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metro.jpg" alt="" title="Metro" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas"></th></tr>
						<tr><th colspan="2"><img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/cable.jpg" alt="" title="Cable" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/metro.jpg" alt="" title="Metro" data-toggle="tooltip" class="iconos-tarifas">+
							<img src="img/integrado.png" alt="" title="Integrado" data-toggle="tooltip" class="iconos-tarifas"></th></tr>
					</table>
				</div>				
			</div>
			<span class="otrasTarifas">Usuarios SIN tarjeta cívica que no gozan beneficios de Integración:</span>
			<br>
			<span class="otrasTarifas">
				<table class="otras">
					<tr class="otras">
					    <th class="otras">Eventual</th>
					    <th class="otras">Univiaje</th>
					  </tr>
					  <tr class="otras">
					    <td class="otras">$2.000</td>
					    <td class="otras">$2.000</td>
				  	</tr>
				</table>
			</span>		
			<br><br>				
			<div id="contenedorIntegrados">
				<h1 class="titulo">Tarifas Rutas Integradas 2015</h1>
				<br><br>
				<div class="boton" id="botonArriba"><div id="flecha-arriba"></div></div>
				<table class="tablaIntegrados">
					<tr><th class="encabezado1 tarifa">Municipio</th><th class="encabezado1 tarifa">Tipo de Tiquete</th>
						<th class="encabezado1 tarifa">Tarifa</th></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">San Antonio de Prado</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">Metrosan - San Javier</td><td class="tarifa">$2.100</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">MEI - Guayabal</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">Floresta - Calasanz</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">Mas. Metro - Castilla</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">San Cristobal</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">Poblado - Laureles</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">La América</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">Blanquizal</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">Robledo</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Medellín</td><td class="tarifa">Tax Maya - Simón Bolívar</td><td class="tarifa">$2.300</td></tr>
					<tr><td class="tarifa">Sabaneta</td><td class="tarifa">Sabaneta</td><td class="tarifa">$2.350</td></tr>
					<!--Esta no es la mejor implementación, no funciona como se espera. Se corregirá más adelante-->
					<tr style="display:none"><td class="tarifa">Envigado</td><td class="tarifa">Envigado</td><td class="tarifa">$2.350</td></tr>
					<tr style="display:none"><td class="tarifa">Envigado</td><td class="tarifa">Envigado - Las Palmas</td><td class="tarifa">$3.250</td></tr>
					<tr style="display:none"><td class="tarifa">Bello</td><td class="tarifa">Integrado Bello</td><td class="tarifa">$2.250</td></tr>
					<tr style="display:none"><td class="tarifa">Bello</td><td class="tarifa">Integrado San Félix</td><td class="tarifa">$3.150</td></tr>
					<tr style="display:none"><td class="tarifa">Caldas</td><td class="tarifa">Micro Caldas</td><td class="tarifa">$2.400</td></tr>
					<tr style="display:none"><td class="tarifa">Copacabana</td><td class="tarifa">Copacabana - Buses</td><td class="tarifa">$2.250</td></tr>
					<tr style="display:none"><td class="tarifa">Girardota</td><td class="tarifa">Bus Girardota</td><td class="tarifa">$2.550</td></tr>
					<tr style="display:none"><td class="tarifa">Girardota</td><td class="tarifa">Micro Girardota</td><td class="tarifa">$2.900</td></tr>
					<tr style="display:none"><td class="tarifa">Barbosa</td><td class="tarifa">Bus Barbosa</td><td class="tarifa">$3.350</td></tr>
					<tr style="display:none"><td class="tarifa">Itagüí</td><td class="tarifa">MEI - Itagüí</td><td class="tarifa">$2.250</td></tr>
					<tr style="display:none"><td class="tarifa">Itagüí</td><td class="tarifa">Circular Itagüí</td><td class="tarifa">$2.250</td></tr>
					<tr style="display:none"><td class="tarifa">La Estrella</td><td class="tarifa">MEI - La Estrella</td><td class="tarifa">$2.250</td></tr>
				</table>
				<div class="boton" id="botonAbajo"><div id="flecha-abajo"></div></div>
			</div>
			<br><br><br>
			<h1 class="tituloContenido">Horarios</h1>
			<br><br>
			<h2 class="tituloHorario">Lunes a Sábado</h2>
			<p style="text-align:center"><b>- 4:30 a.m. a 11:00 p.m.</b></p>
			<br><br>
			<h2 class="tituloHorario">Domingos y Festivos</h2>
			<p style="text-align:center"><b>- Líneas A, B, 1 y 2: </b> 5:00 a.m. a 10:00 p.m.</p>
			<p style="text-align:center"><b>- Línea K: </b> 8:30 a.m. a 10:00 p.m.</p>
			<p style="text-align:center"><b>- Línea J: </b> 9:00 a.m. a 10:00 p.m.</p>
			<br><br>
			<h2 class="tituloHorario">Cable Arví (Línea L)</h2>
			<p style="text-align:center"><b>- 9:00 a.m. a 10:00 p.m.</b></p>
			<p style="text-align:center"><b> <i>No presta servicio el primer día hábil de la semana</i> </b></p>
			<br><br><br><br>
		</section>
		<footer></footer>
	</div>
</body>

</html>
