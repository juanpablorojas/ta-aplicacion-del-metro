document.addEventListener('DOMContentLoaded', function() 
{
   var primeraTarifa = 0;
   var ultimaTarifa = 11;
   var arrayTarifas = document.getElementsByClassName('tablaIntegrados').item(0).rows;
   var flechaArriba = document.getElementById('botonArriba');
   var flechaAbajo = document.getElementById('botonAbajo');

   function moverListaArriba (evt) 
   {   	   		
   		if (primeraTarifa > 0) 
		{
			primeraTarifa --;
	   		arrayTarifas[primeraTarifa].style.display = "";   		  		
	   		arrayTarifas[ultimaTarifa].style.display = "none";
	   		ultimaTarifa --;
		}   		
   }

   function moverListaAbajo (evt) 
   {   	
	   	if (ultimaTarifa < arrayTarifas.length-1) 
		{
			arrayTarifas[primeraTarifa].style.display = "none";   		
	   		primeraTarifa ++;
	   		ultimaTarifa ++;
	   		arrayTarifas[ultimaTarifa].style.display = "";
		}   		
   }


   flechaArriba.addEventListener('click',moverListaArriba,false);
   flechaAbajo.addEventListener('click',moverListaAbajo,false);
});