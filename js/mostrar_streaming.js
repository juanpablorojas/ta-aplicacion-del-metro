document.addEventListener('DOMContentLoaded', function() 
{
   var ulEstaciones = document.getElementById('lista-estaciones');
   var arrayEstaciones = ulEstaciones.children;
   var imgStreaming = document.getElementById('img');
   var informacion = document.getElementById('info');

   function mostrarVideo () {  
      imgStreaming.className = "mostrar";
      informacion.className = "mostrar";
      var websocket = io.connect("http://localhost:3000");
      websocket.on("stream",proc);
      function proc (img) {
         imgStreaming.src = img;
      }
      websocket.on("info",showInfo);
      function showInfo (info) {
         // informacion.innerHTML = "El porcentaje de congestion en esta estacion es: " + info;
         informacion.innerHTML = info;
      }
   }

   for (var i = 0; i < arrayEstaciones.length; i++) {
      var li = arrayEstaciones[i];
      li.addEventListener("click", mostrarVideo, false);
   }
});