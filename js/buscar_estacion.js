document.addEventListener('DOMContentLoaded', function() 
{
   var primeraEstacion = 0;
   var ultimaEstacion = 9;
   var ulEstaciones = document.getElementById('lista-estaciones');
   var arrayEstaciones = ulEstaciones.children;
   var flechaArriba = document.getElementById('boton-arriba');
   var flechaAbajo = document.getElementById('boton-abajo');

   function moverListaArriba (evt) 
   {   	   		
   		if (primeraEstacion > 0) 
		{
			   primeraEstacion --;
	   		arrayEstaciones[primeraEstacion].style.display = "block";   		  		
	   		arrayEstaciones[ultimaEstacion].style.display = "none";
	   		ultimaEstacion --;
		}   		
   }

   function moverListaAbajo (evt) 
   {   	
	   	if (ultimaEstacion < arrayEstaciones.length-1) 
		{
			arrayEstaciones[primeraEstacion].style.display = "none";   		
	   		primeraEstacion ++;
	   		ultimaEstacion ++;
	   		arrayEstaciones[ultimaEstacion].style.display = "block";
		}   		
   }


   flechaArriba.addEventListener('click',moverListaArriba,false);
   flechaAbajo.addEventListener('click',moverListaAbajo,false);
});