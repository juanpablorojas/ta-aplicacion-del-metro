$(document).ready(function () {

   var imagen = $('#img-zoom');
   var zoom = $('#zoom');
   var botones_zoom = $('.boton-zoom');
   var boton_zoom_in = $('#zoom-in');
   var boton_zoom_out = $('#zoom-out');
   var boton_cerrar = $('#cerrar-zoom');

/**
** Bloque que realiza el zoom de la imagen seleccionada
*/

   var width = 98;
   var height = 95;
      
   $('.mapa').click(function(){
      imagen.attr('src', $(this).attr('src'));  
      zoom.css({'display': 'block', 'z-index': '10'});
      botones_zoom.css({'display': 'block', 'z-index': '99'});
      boton_cerrar.css({'display': 'block', 'z-index': '99'});
      imagen.css(
      {          
         'top': '12px',
         'left' : '12px',
         'width': '98%',
         'height': '95%',
         'border': '1px solid black',
         'z-index': '12'
      });  
      $('body').css('overflow-y','hidden');   
   });

   boton_cerrar.click(function()
   {
      width = 98;
      height = 95;
      zoom.css('display', 'none');
      botones_zoom.css({'display': 'none', 'z-index': '3'});
      boton_cerrar.css({'display': 'none', 'z-index': '3'});
      $('body').css('overflow-y','scroll');
   });   

   boton_zoom_in.click(function () {

      if (width < 200 && height < 200) {
         width += 50;
         height += 50;
         TweenLite.to(imagen, 1, {width: width+'%', height: height+'%'});
      }
   });

    boton_zoom_out.click(function () {

      if (width > 100 && height > 100) {
         width -= 50;
         height -= 50;
         TweenLite.to(imagen, 1, {width: width+'%', height: height+'%'});
      }
   });

Draggable.create(imagen, {type:"x,y", edgeResistance:0.65, bounds:zoom, throwProps:true});

/**
** Bloque que permite moverse por la imagen sobre la que se hizo el zoom
*/

/*function iniciar_movimiento (evt) {

   var x_anterior = 0;
   var y_anterior = 0;
   var top = imagen.offset().top;
   var left = imagen.offset().left;
   var bott = imagen.offset().bottom;
   var right = imagen.offset().right;

   zoom.bind('mousemove',function(e){ 
      console.log("e.pageX: " + e.pageX + ", e.pageY: " + e.pageY);
      if (e.pageX > x_anterior) 
      {
         x_anterior = e.pageX;
         left+=3.7;
      }
      if (e.pageX < x_anterior) 
      {
         x_anterior = e.pageX;
         left-=3.7;
      }
      if (e.pageY > y_anterior) 
      {
         y_anterior = e.pageY;
         top+=3.7;
      }
      if (e.pageY < y_anterior) 
      {
         y_anterior = e.pageY;
         top-=3.7;
      }
      $(imagen).offset({top: top, left: left});
   });
   
   /*var position = imagen.position();   
   position.left;
   position.top;*/
/*}

function terminar_movimiento (evt) {
    zoom.unbind('mousemove');        
}

imagen.mousedown(iniciar_movimiento);
imagen.mouseup(terminar_movimiento);*/

});