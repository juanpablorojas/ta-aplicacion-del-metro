/*
    Efecto de la publicidad en la pagina
*/
function advertisement(){
    TweenLite.to(message1, 2, {'opacity':'1'});
    TweenLite.to(message1, 2, {'opacity':'0'}).delay(6);
    TweenLite.to(message2, 2, {'opacity':'1'}).delay(8);
    TweenLite.to(message2, 2, {'opacity':'0'}).delay(15);
    TweenLite.to(message3, 2, {'opacity':'1'}).delay(17);
    TweenLite.to(message3, 2, {'opacity':'0'}).delay(24);
    TweenLite.to(message4, 2, {'opacity':'1'}).delay(26);
    TweenLite.to(message4, 2, {'opacity':'0'}).delay(32);
}

function loadedPage(evnt) 
{
    var message1 = document.getElementById('message1');
    var message2 = document.getElementById('message2');
    var message3 = document.getElementById('message3');
    var message4 = document.getElementById('message4');

    advertisement();
    
    setInterval(function () {

        advertisement();

    }, 34000);
}

document.addEventListener('DOMContentLoaded',loadedPage);