module.exports = function (grunt) {
	grunt.initConfig({
		//Definicion de las tareas
		/*clean: {
		  js: ["path/to/dir/*.js", "!path/to/dir/*.min.js"]
		}*/
		watch: {
		  css: {
		    files: '**/*.scss',
		    tasks: ['sass'],
		    options: {
		      livereload: true,
		    },
		  },
		},

		sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'main.css': 'main.scss'
                }
            }
        },
	});

	//Antes de todo esto es necesario instalar la dependencia,
	//es decir, ejecutar el siguiente comando npm install grunt-contrib-clean --save-dev

	//Carga de la tarea
	grunt.loadNpmTasks('grunt-contrib-clean');
	//grunt.loadNpmTasks('grunt-contrib-concat');
	//grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');

	//Registro de la tarea para su posterior uso
	grunt.registerTask('default', ['clean']);//Tarea por omision

}