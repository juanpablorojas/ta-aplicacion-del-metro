<? session_start() ?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="js/animaciones.js"></script>
	<script src="js/vendor/TweenMax.min.js"></script>
	<script src="js/vendor/jquery-1.11.3.min.js"></script>
	<link rel="stylesheet" href="css/monitoreo.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="js/buscar_estacion.js"></script>
	<script src="js/mostrar_streaming.js"></script>
	<script src="http://localhost:8081/node_modules/socket.io/node_modules/socket.io-client/socket.io.js"></script>
</head>
<body>
	<div id="contenidosUser">
		<span class="log" style="margin-right:10%" id="saludoLogin" onclick="window.location='scripts php/redir.php'">
			<?php
				if($_SESSION['username']): echo "¡Hola ".$_SESSION['username'].'!';
				else: echo "Iniciar Sesión";
				endif;
			?>
		</span>
		<span class="log" style="margin-left:10%" id="cerrarRegistro" onclick="window.location='scripts php/redirSesion.php'">
			<?php
				if($_SESSION['username']): echo "Cerrar Sesión";
				else: echo "Registrarse";
				endif;
			?>
		</span>
	</div>
	<div id="contenedor-monitoreo">
		<header class="row">
			<div id="marca1" class="marcaAnimada col-sm-4"></div>			
			<div id="titulo" class="col-sm-4"><p><span id="titulo-1">Metro de Medellin</span></br><span id="titulo-2">Calidad de vida</span></p></div>
			<div id="marca2" class="marcaAnimada col-sm-4"></div>
		</header>
		<div class="division row">
			<br>
			<div id="icono1" class="iconos col-sm-4"><b><p class="texto">Investigamos e innovamos en compañía de universidades y proveedores locales (I+D+i)</p></b></div>
			<div id="icono2" class="iconos col-sm-4"><b><p class="texto">El METRO va más allá de ser un medio de transporte; es una forma de vida y un espacio para el encuentro (RSE)</p></b></div>
			<div id="icono3" class="iconos col-sm-4"><b><p class="texto">Somos un Sistema socialmente incluyente y ambientalmente sostenible (Movilidad Sostenible)</p></b></div>			
		</div>	
		<nav>
			<a href="index.php">Inicio</a>
			<a href="tarifas_y_horarios.php">Tarifas y horarios</a>
			<a href="monitoreo.php">Monitoreo</a>
			<a href="beneficios.php">Beneficios</a>
		</nav>
		<section>
			<h1 class="tituloContenido">¡Monitoreo de Estaciones en Tiempo Real!</h1>
			<div id="main-content">
				<aside>			
					<div id="contenedor-estaciones">
						<h3>Así se ve la estación en este momento</h3>
						<div class="boton" id="boton-arriba"><div id="flecha-arriba"></div></div>
						<div id="contenedor-lista">
							<ul id="lista-estaciones">
								<li class="estacion">Niqu&iacutea</li>
								<li class="estacion">Bello</li>
								<li class="estacion">Madera</li>
								<li class="estacion">Acevedo</li>
								<li class="estacion">Tricentenario</li>
								<li class="estacion">Caribe</li>
								<li class="estacion">Universidad</li>
								<li class="estacion">Hospital</li>
								<li class="estacion">Prado</li>
								<li class="estacion">Parque Berr&iacuteo</li>
								<li class="estacion">San Antonio</li>
								<li class="estacion">Alpujarra</li>
								<li class="estacion">Exposiciones</li>
								<li class="estacion">Industriales</li>
								<li class="estacion">Poblado</li>
								<li class="estacion">Aguacatala</li>
								<li class="estacion">Ayur&aacute</li>
								<li class="estacion">Envigado</li>
								<li class="estacion">Itagu&iacute</li>
								<li class="estacion">Sabaneta</li>
								<li class="estacion">La Estrella</li>
								<li class="estacion">San Antonio</li>
								<li class="estacion">Cisneros</li>
								<li class="estacion">Suramericana</li>
								<li class="estacion">Estadio</li>
								<li class="estacion">Floresta</li>
								<li class="estacion">San Javier</li>
							</ul>
						</div>
						<div class="boton" id="boton-abajo"><div id="flecha-abajo"></div></div>
					</div>
				</aside>
				<div id="contenedor-streaming">
					<img alt="STREAM" id="img" class="esconder"><br>
					<span id="info" class="esconder">Esperando al servidor para mostrar congestión... </span>
				</div>
				<br><br>
				<button type="button" onclick="mostrar()" class="btn btn-primary btn-lg">Bonificar</button>
				<br><br><br>
				<div class="esconder" id="cont" style="text-align:center">
					<form action="scripts php/bonificar.php" method="post">
						<span style="font-size:18px">Ingresa la hora aproximada en la que estarás en la estación</span><br>
						<span style="font-size:18px">Hora: </span><input name="hora" id="hora" style="width:30px" type="text">
						<span style="font-size:18px"> Min: </span><input name="min" id="min" style="width:30px" type="text"><br><br>
						<input type="submit" value="Enviar" onclick="bonificar()" class="btn btn-primary">
					</form>
				</div>
				<br><br>
				<?php
					if($_SESSION['username']): echo "<script>$('button')[0].disabled=false</script>";
					else: echo "<script>$('button')[0].disabled=true</script>";
					endif;
					echo "<script>function mostrar(){
									$('#cont')[0].className = 'mostrar';
								}
								function bonificar(){
									var moment = new Date();
									moment.setHours($('#hora').val(), $('#min').val());
									alert('Tu bonificación se efectuará a las '+moment.getHours()+':'+moment.getMinutes());
								}
					</script>"
				?>
			</div>			
		</section>
		<footer></footer>
	</div>
</body>
</html>