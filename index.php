<? session_start() ?>
<!doctype html>
<html class="no-js" lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Metro de Medellin</title>
	<meta name="description" content="">
   	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/index.css">
	<link rel="stylesheet" href="css/publicidad.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="js/jquery.slides.js"></script>
	<script src="js/img_zoom.js"></script>
	<script src="js/vendor/TweenMax.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.17.0/utils/Draggable.min.js"></script>
	<script type="text/javascript" src="js/animaciones.js"></script>
</head>
<body>
	<!--Barra de Inicio de sesión o Perfil-->
	<div id="contenidosUser">
		<span class="log" style="margin-right:10%" id="saludoLogin" onclick="window.location='scripts php/redir.php'">
			<?php
				if($_SESSION['username']): echo "¡Hola ".$_SESSION['username'].'!';
				else: echo "Iniciar Sesión";
				endif;
			?>
		</span>
		<span class="log" style="margin-left:10%" id="cerrarRegistro" onclick="window.location='scripts php/redirSesion.php'">
			<?php
				if($_SESSION['username']): echo "Cerrar Sesión";
				else: echo "Registrarse";
				endif;
			?>
		</span>
	</div>
	<div id="contenedor-index">
		<header class="row">
			<div id="marca1" class="marcaAnimada col-sm-4"></div>			
			<div id="titulo" class="col-sm-4"><p><span id="titulo-1">Metro de Medellin</span></br><span id="titulo-2">Calidad de vida</span></p></div>
			<div id="marca2" class="marcaAnimada col-sm-4"></div>
		</header>
		<div class="division row">
			<br>
			<div id="icono1" class="iconos col-sm-4"><b><p class="texto">Investigamos e innovamos en compañía de universidades y proveedores locales (I+D+i)</p></b></div>
			<div id="icono2" class="iconos col-sm-4"><b><p class="texto">El METRO va más allá de ser un medio de transporte; es una forma de vida y un espacio para el encuentro (RSE)</p></b></div>
			<div id="icono3" class="iconos col-sm-4"><b><p class="texto">Somos un Sistema socialmente incluyente y ambientalmente sostenible (Movilidad Sostenible)</p></b></div>			
		</div>	
		<nav>
			<a href="index.php">Inicio</a>
			<a href="tarifas_y_horarios.php">Tarifas y horarios</a>
			<a href="monitoreo.php">Monitoreo</a>
			<a href="beneficios.php">Beneficios</a>
		</nav>
		<section>			
			<div id="contenedor">
				<h1 class="tituloContenido">Mapa SITVA</h1>
				<div class="slides">
					<img class="mapa imagenes" src="img/mapa_metro.gif" alt="Mapa metro">
					<img class="mapa imagenes" src="img/mapa-esquematico.jpg" alt="Mapa metro">
					<img class="mapa imagenes" src="img/mapa-sitva.png" alt="Mapa metro">
				</div>
				<div id="zoom">	
					<img src="""" alt="Zoom de imagen" id="img-zoom" class="dragme">				
				</div>
				<div id="cerrar-zoom">X</div>
				<div class="boton-zoom" id="zoom-in">+</div>
				<div class="boton-zoom" id="zoom-out">-</div>
			
		</section>
		
		<footer></footer>
	</div>
	

	
</body>
</html>